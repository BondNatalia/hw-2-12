// 1. тому що в input  дані можуть вводитись не тільки за допомогою клавіатури

document.addEventListener("keydown", (ev) => {

    const btnArr = document.getElementsByClassName("btn");

    for (const btn of btnArr){
        btn.style.backgroundColor = "black";
        console.log(ev.code)
        if(btn.dataset.key === ev.code) {
            btn.style.backgroundColor = "blue";
        }
    }
})